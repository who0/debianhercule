# Debian on Hercules

## SDL Hercules 4.x Hyperion
(http://www.softdevlabs.com/hyperion.html)

*Hercules* is an open source software implementation of the mainframe *System/370* and *ESA/390* architectures, in addition to the latest 64-bit *z/Architecture*. Hercules runs under <code>Linux</code>, <code>Windows</code>, <code>Solaris</code>, <code>FreeBSD</code>, and <code>Mac OS X</code>.



## Steps

  1. Install Hyperion/Hercule
  1. Download debian bootable files
  1. Create Virtual disk
  1. Create config file
  1. IPL 00C
  1. Install
  1. IPL 120



__IPL__: IPL (initial program load) is a mainframe term for the loading of the operating system into the computer's main memory . A mainframe operating system (such as OS/390) contains many megabytes of code that is customized by each installation, requiring some time to load the code into the memory.

__DASD__: A direct-access storage device is a secondary storage device in which "each physical record has a discrete location and a unique address"


## Installation
```bash
git clone https://github.com/SDL-Hercules-390/hyperion.git
./configure && make 
```

## Download Debian bootable files
You need files from netinstall.
http://ftp.debian.org/debian/dists/stable/main/installer-s390x/current/images/generic/

* kernel.debian (kernel)
* initrd.debian (initram)
* parmfile.debian

Save files in <code>rdr</code> folder

## Create virtual Disk

```bash
dasdinit -lfs -linux 3390.LINUX.0120 3390-3 LIN120
```
env 2Go 


## Configuration
Create <code>debian.cnf</code>, you can adjust carefully.
(https://framagit.org/who0/debianhercule/-/blob/master/debian.cnf)

```conf
CPUSERIAL 000069        # CPU serial number
CPUMODEL  9672          # CPU model number
MAINSIZE  256           # Main storage size in megabytes
XPNDSIZE  0             # Expanded storage size in megabytes
CNSLPORT  3270          # TCP port number to which consoles connect
NUMCPU    2             # Number of CPUs
OSTAILOR  LINUX         # OS tailoring
PANRATE   SLOW          # Panel refresh rate (SLOW, FAST)
ARCHMODE  ESAME         # Architecture mode ESA/390 or ESAME

# .-----------------------Device number
# |     .-----------------Device type
# |     |       .---------File name and parameters
# |     |       |
# V     V       V
#---    ----    --------------------

# console
001F    3270

# terminal
0009    3215

# reader
000C    3505    ./rdr/kernel.debian ./rdr/parmfile.debian ./rdr/initrd.debian autopad eof

# printer
000E    1403    ./prt/print00e.txt crlf

# dasd (one disk) 
0120    3390    ./3390.LINUX.0120
#0121    3390    ./dasd/3390.LINUX.0121

# tape
0581    3420

# network                               s390     realbox
0A00,0A01  CTCI -n /dev/net/tun -t 1500 10.1.1.2 10.1.1.1

```

## IPL 00C
```bash
hercule -f debian.cnf
```
```
herc =====> ipl c
```
Install should start. ( input must be prefixed by '.' )
ex:  <code>.10.1.1.2</code>

![alt text](https://framagit.org/who0/debianhercule/-/raw/master/start.png "startup")

## Install
### Network config
* ctc: Channel to channel <code>.1</code>
* ctc read device: 0.0.0a00 <code>.1</code>
* ctc write device: 0.0.0a01 <code>.2</code>
* S/390 <code>.1</code>

### Ip

* ip <code>.10.1.1.2</code>
* mask <code>.255.255.255.0</code>
* gw <code>.10.1.1.1</code>
* dns <code>.8.8.8.8</code>
* hostname <code>.mainframe</code>
* dnsname <code>..</code> (yes 2 dots )


### SSH
* Create password <code>.password</code>
* open new terminal and ssh installer@10.1.1.2

And Install your Debian, use DASD device.
Zipl will create bootable disk.

## IPL 120
After install ... reboot or poweroff, <code>quit</code> 
```bash
hercule -f debian.cnf
```
herc =====> <code>ipl 120</code> should start your debian.


## Common Errors
* Add hercules binaries in your PATH.
* double check path in <code>debian.cnf</code>



